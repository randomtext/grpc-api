module gitlab.com/random-text/grpc

go 1.16

require (
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	go.mongodb.org/mongo-driver v1.5.4
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
