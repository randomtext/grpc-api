package payloads

import "go.mongodb.org/mongo-driver/bson/primitive"

type Text struct {
	ID       primitive.ObjectID `bson:"_id" json:"id" validate:"required"`
	Text     string             `bson:"text" json:"text" validate:"required"`
	AuthorID string             `bson:"author_id" json:"author_id" validate:"required"`
}
