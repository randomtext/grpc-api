package server

import (
	"gitlab.com/random-text/grpc/src/payloads"
)

type UseCase interface {
	NewText(p *payloads.Text) error
}
