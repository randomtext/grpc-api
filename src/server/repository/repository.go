package repository

import (
	"context"
	"gitlab.com/random-text/grpc/src/server"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
)

type repository struct {
	C *mongo.Client
}

func New(c *mongo.Client) server.Repository {
	return &repository{C: c}
}

func(r *repository) UpdateTextStatus(filter, update bson.M) error {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	_, err := coll.UpdateOne(context.TODO(), filter, bson.M{"$set": update})
	if err != nil {
		return err
	}
	return nil
}
