package grpc

import (
	"context"
	"gitlab.com/random-text/grpc/src/payloads"
	"gitlab.com/random-text/grpc/src/server"
	"gitlab.com/random-text/grpc/src/server/protos"
	"gitlab.com/random-text/grpc/src/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"net/http"
)

type Server struct {
	U server.UseCase
}

func NewGRPC(u server.UseCase, g grpc.ServiceRegistrar) {
	protos.RegisterServerServer(g, &Server{U: u})
}

func (s Server) NewText(_ context.Context, payload *protos.TextPayload) (*protos.TextResponse, error) {
	res := &protos.TextResponse{Success: "fail"}
	id, err := primitive.ObjectIDFromHex(payload.Id)
	if err != nil {
		utils.Log(true, err.Error())
		return res, utils.Error(http.StatusInternalServerError, "something went wrong", map[string]string{})
	}
	text := &payloads.Text{
		ID:       id,
		Text:     payload.Message,
		AuthorID: payload.AuthorId,
	}
	if validation := utils.Validate(text, nil); validation != nil {
		utils.Log(true, validation.Error())
		return res, validation
	}
	err = s.U.NewText(text)
	if err != nil {
		return res, err
	}
	res.Success = "success"
	return res, nil
}
