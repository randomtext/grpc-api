package usecase

import (
	"gitlab.com/random-text/grpc/src/payloads"
	"gitlab.com/random-text/grpc/src/server"
	"gitlab.com/random-text/grpc/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

type useCase struct {
	R server.Repository
}

func New(r server.Repository) server.UseCase {
	return &useCase{R: r}
}

func(u *useCase) NewText(p *payloads.Text) error {
	filter := bson.M{ "_id": p.ID }
	update := bson.M{"grpc_status": "GRPC ok"}
	err := u.R.UpdateTextStatus(filter, update)
	if err != nil {
		utils.Log(true, err.Error())
		return utils.Error(http.StatusInternalServerError, "something went wrong", map[string]string{})
	}
	return nil
}
