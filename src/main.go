package main

import (
	"context"
	"fmt"
	"gitlab.com/random-text/grpc/src/connections"
	"gitlab.com/random-text/grpc/src/server/delivery/grpc"
	"gitlab.com/random-text/grpc/src/server/repository"
	usecase2 "gitlab.com/random-text/grpc/src/server/usecase"
	"go.mongodb.org/mongo-driver/mongo"
	grpc2 "google.golang.org/grpc"
	"net/http"
	"os"
)

func initGRPC(c *mongo.Client, g grpc2.ServiceRegistrar) {
	repo := repository.New(c)
	usecase := usecase2.New(repo)
	grpc.NewGRPC(usecase, g)
}

func main() {
	c, err := connections.MongoInit()
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := c.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	g, s := connections.GRPCInit()
	initGRPC(c, g)
	http.DefaultServeMux.Handle("/", connections.WriteOK())
	if os.Getenv("ENVIRONMENT") == "production" {
		fmt.Println("grpc is running in production mode on port " + os.Getenv("GRPC_PORT"))
		if err := http.ServeTLS(s,
			connections.GrpcHandlerFunc(g, http.DefaultServeMux),
			os.Getenv("CERT_FILE"),
			os.Getenv("KEY_FILE")); err != nil {
			panic(err)
		}
	}
	fmt.Println("grpc is running in development mode on port " + os.Getenv("GRPC_PORT"))
	if err := g.Serve(s); err != nil {
		panic(err)
	}
}
