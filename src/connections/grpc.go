package connections

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

func WriteOK () http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		return
	}
}

func GrpcHandlerFunc(g *grpc.Server, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ct := r.Header.Get("Content-Type")
		if r.ProtoMajor == 2 && strings.Contains(ct, "application/grpc") {
			g.ServeHTTP(w, r)
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

func unaryInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	log.Println("--> unary interceptor: ", info.FullMethod)
	return handler(ctx, req)
}

func GRPCInit() (*grpc.Server, net.Listener) {
	if os.Getenv("ENVIRONMENT") == "production" {
		creds, err := credentials.NewServerTLSFromFile(os.Getenv("CERT_FILE"), os.Getenv("KEY_FILE"))
		if err != nil {
			panic(err)
		}
		lis, err := net.Listen("tcp", ":"+os.Getenv("GRPC_PORT"))
		if err != nil {
			panic(err)
		}
		s := grpc.NewServer(
			grpc.Creds(creds),
			grpc.UnaryInterceptor(unaryInterceptor),
		)
		reflection.Register(s)
		return s, lis
	}
	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPC_PORT"))
	if err != nil {
		panic(err)
	}
	s := grpc.NewServer(
		grpc.UnaryInterceptor(unaryInterceptor),
	)
	reflection.Register(s)
	return s, lis
}
